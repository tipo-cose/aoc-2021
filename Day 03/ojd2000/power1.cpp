#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using std::string;

int main()
{
    auto in = std::ifstream{ "input" };
    string line;
    int gamma = 0, epsilon = 0;
    
    if (in >> line)
    {
        auto zero_counter = std::vector(line.size(), 0);
        size_t line_cnt = 0;
    
        do
        {
            for (int i = 0; i < line.size(); i++)
                zero_counter[i] += line[i] == '0';
            
            line_cnt++;
        } while (in >> line);

        size_t pow = 1;

        for (auto it = zero_counter.crbegin();
             it != zero_counter.crend();
             it++)
        {
            int& inc = *it > line_cnt / 2
                        ? epsilon
                        : gamma;

            inc += pow;
            pow *= 2;
        }
    }

    std::cout << gamma * epsilon << std::endl;
    return 0;
}