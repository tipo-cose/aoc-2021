#include <algorithm>
#include <array>
#include <concepts>
#include <fstream>
#include <iostream>
#include <iterator>
#include <ranges>
#include <string>
#include <vector>
#include <cmath>

namespace ranges = std::ranges;
namespace views = std::views;

using std::string;
using vecstr = std::vector<string>;

/**
 * @brief Groups range_from's elements into range_to by the i-th digit.
 * It is a counting sort on the i-th digit, kinda like what is used in radix sort.
 * 
 * @param range_from Elements to order/group
 * @param range_to Destination of the ordered elements.
 * @param i Ordinal number for digit
 * @return auto Iterator in range_to pointing to first one inserted.
 */
auto counting_sort(ranges::range auto&& range_from,
                   ranges::range auto&& range_to,
                   size_t i)
{
    auto zeroes_begin_to = range_to.begin();
    auto ones_begin_to = range_to.begin()
        + ranges::count_if(range_from,
            [ i ] (auto&& str)
            {
                return str[i] == '0';
            });
    
    for (auto& str : range_from)
        if (str[i] == '0')
            *zeroes_begin_to++ = std::move(str);
        else
            *ones_begin_to++ = std::move(str);
    
    return zeroes_begin_to;
}

/**
 * @brief Returns a pair of ranges for, respectively, oxygen and cdioxide.
 * 
 * @param values std::vector of strings, ordered by first digit.
 * @param split_iter Iterator to first "one" element in the vector.
 * @return auto Pair of ranges (oxygen, cdioxide).
 */
inline auto first_ranges(vecstr& values,
                         std::forward_iterator auto split_iter)
{
    const auto first_part = ranges::subrange{
        values.begin(),
        split_iter
    };
    const auto second_part = ranges::subrange{
        split_iter,
        values.end()
    };
    
    return values.end() - split_iter >= std::ceil(values.size() / 2)
        ? std::make_pair(second_part, first_part)
        : std::make_pair(first_part, second_part);
}

/**
 * @brief Returns a part of the range values, ready for next-column sorting.
 * 
 * @param values Source range, assumed ordered by some i-th digit.
 * @param split_iter Iterator inside values pointing to the first "one"-digit element.
 * @param most_common Whether to return the range of most or least common digit.
 * @return auto A subrange, either (values.begin(), split_iter) or (split_iter, values.end()).
 */
inline auto next_range(ranges::range auto&& values,
                       std::forward_iterator auto split_iter,
                       bool most_common)
{
    const auto ones_majority = values.end() - split_iter >= std::ceil(values.size() / 2);

    return ones_majority ^ most_common
        ? ranges::subrange{ values.begin(), split_iter }
        : ranges::subrange{ split_iter, values.end() };
}

/**
 * @brief Convert a binary string into integer.
 * 
 */
auto bin_to_dec(const string& bin)
{
    size_t res = 0;
    size_t pow = 1;

    for (auto ch : bin | views::reverse)
    {
        if (ch == '1')
            res += pow;
        
        pow *= 2;
    }
    
    return res;
}

int main()
{
    auto in = std::ifstream{ "test" };
    auto values = vecstr{};
    
    // Read from file
    ranges::copy(ranges::istream_view<std::string>(in),
                 std::back_inserter(values));
    
    // Auxiliary vector for counting sort's source/destination (alternating)
    auto aux_vec = vecstr(values.size());
    
    const auto ones_begin = counting_sort(values, aux_vec, 0);
    auto [oxygen, cdioxide] = first_ranges(aux_vec, std::move(ones_begin));

    // Scroll columns
    for (auto i : views::iota((size_t) 1, values.front().size()))
    {
        // debug
        for (auto& s : oxygen)
            std::cout << "." << s << "." << std::endl;
        std::cout << std::endl;

        // debug
        for (auto& s : cdioxide)
            std::cout << "," << s << "," << std::endl;
        std::cout << std::endl << std::endl;
        
        if (oxygen.size() > 1)
        {
            const auto ones_begin_oxygen = counting_sort(
                oxygen,
                values,
                i);

            // Assign new subrange (only needed elements, not more)
            oxygen = next_range(
                ranges::subrange{ values.begin(),
                                  values.begin() + oxygen.size() },
                ones_begin_oxygen,
                true
            );
        }
        
        if (cdioxide.size() > 1)
        {
            // starting destination point directly after oxygen's last element
            const auto ones_begin_cdioxide = counting_sort(
                cdioxide,
                ranges::subrange(values.begin() + oxygen.size(),
                                values.end()),
                i);

            // Assign new subrange (only needed elements, directly after oxygen's)
            cdioxide = next_range(
                ranges::subrange{ values.begin() + oxygen.size(),
                                  values.begin() + oxygen.size() + cdioxide.size() },
                ones_begin_cdioxide,
                false
            );
        }
        
        // One element remaining
        if (oxygen.size() == 1 && cdioxide.size() == 1)
            break;

        // Swap pointers, for allowing "old sorted source" -> "new sorted destination"
        std::swap(values, aux_vec);
    }

    std::cout << bin_to_dec(oxygen[0]) * bin_to_dec(cdioxide[0]) << std::endl;
    return 0;
}