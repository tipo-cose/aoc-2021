import Data.List

bintodec 0 = Just 0
bintodec i | last < 2 = fmap (\x -> 2*x + last) (bintodec (div i 10))
           | otherwise = Nothing
    where last = mod i 10

fromMaybe (Just i) = i

btd = fromMaybe . bintodec . (read::String->Int)

count :: Eq a => a -> [a] -> Int
count x xs = (length . filter (== x)) xs

most :: String -> Char
most a
  | (count '0' a) > (count '1' a) = '0'
  | otherwise                     = '1'

least :: String -> Char
least a = if (most a) == '0'
  then '1'
  else '0'

gas :: [String] -> (String -> Char) -> String
gas l f = recGas l f 0
  where recGas l f n =
          if length l == 1
          then
            head l
          else
            recGas (filter (\s -> s !! n == (f ((transpose l)!!n))) l) f $ n + 1

solve :: [String] -> Int
solve l = (btd $ gas l most) * (btd $ gas l least)

main = readFile "input" >>= print . solve . lines
