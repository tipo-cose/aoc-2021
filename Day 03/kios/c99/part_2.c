#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int prefix(const char* pre, const char* str){
    if (strlen(pre) == 0){
        return 1;
    }
    return strncmp(pre, str, strlen(pre)) == 0;
}

int oxygen(char data[][64], size_t len, size_t bits, size_t pos, char* filter,int acc){
    if (bits == pos) 
        return acc;
   
    printf("bit %d, filtering on: %s", pos, filter);
    int of = 0, zf = 0, o = 0;
    for (size_t i = 0; i < len; i++){
        if (!prefix(filter, data[i]))
            continue;

       if (data[i][pos] == '1')
            of++;
        else 
            zf++;
    }


    if (of >= zf)
        o = 1;
    
    filter[pos] = '0' + o;
    filter[pos+1] = 0;
    
        
    printf("-- recap bit %d: %d, %d, %d\n", pos, of, zf, 10 * acc + o);
    return oxygen(data, len, bits, pos + 1, filter, 2 * acc + o);
}

int co2(char data[][64], size_t len, size_t bits, size_t pos, char* filter,int acc){
    if (bits == pos) 
        return acc;
   
    printf("bit %d, filtering on: %s", pos, filter);
    int of = 0, zf = 0, o = 0;
    for (size_t i = 0; i < len; i++){
        if (!prefix(filter, data[i]))
            continue;

       if (data[i][pos] == '1')
            of++;
        else 
            zf++;
    }
    

    if (of < zf)
        o = 1;

    if (of == 1 && zf == 0)
        o = 1;
    else if (of == 0 && zf == 1)
        o = 0;

    
    filter[pos] = '0' + o;
    filter[pos+1] = 0;
    
        
    printf("-- recap bit %d: %d, %d, %d\n", pos, of, zf, 10 * acc + o);
    return co2(data, len, bits, pos + 1, filter, 2 * acc + o);

}


int main(int argc, char *argv[])
{
    FILE *in_file = fopen(argv[1], "r");

    char data[10000][64];
    size_t len;

    while (fscanf(in_file, "%s", data[len++]) != EOF);
    len--;

    size_t bits = strlen(data[0]);
   
    char filter[64];
    filter[0] = 0;
    int o = oxygen(data, len, bits, 0, filter, 0);
    printf("%d \n", o);
    filter[0] = 0;
    int c = co2(data, len, bits, 0, filter, 0);
    printf("%d \n", c);
    
    int pc = c * o;

    printf("power consumption: %d \n", pc);

    fclose(in_file);
    exit(EXIT_SUCCESS);
}

