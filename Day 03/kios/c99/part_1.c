#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int prefix(const char* pre, const char* str){
    if (strlen(pre) == 0)
        return 1;
    return strncmp(pre, str, strlen(pre)) == 0;
}

int Gamma(char data[][64], size_t len, size_t bits, size_t pos, char* filter,int acc){
    if (bits == pos) 
        return acc;
   
    printf("bit %d, filtering on: %s\n", pos, filter);
    int of = 0, zf = 0, o = 0;
    for (size_t i = 0; i < len; i++){
        if (prefix(filter, data[i]))
            continue;

        printf("%s\n", data[i]);

        if (data[i][pos] == '1')
            of++;
        else 
            zf++;
    }


    if (of >= zf)
        o = 1;
    
    filter[pos] = '0' + o;
    filter[pos+1] = 0;
    
        
    printf("-- recap bit %d: %d, %d, %d\n", pos, of, zf, 10 * acc + o);
    return Gamma(data, len, bits, pos + 1, filter, 10 * acc + o);
}

int Epsilon(char data[][64], size_t len){
    size_t sample_len = strlen(data[0]); 
    int e = 0;
    for (size_t i = 0; i < sample_len; i++){
        int one_f = 0, zero_f = 0;
        for (size_t j = 0; j < len; j++){
            if (data[j][i] == '1')
                one_f++;
            else
                zero_f++;
        }
        if (one_f < zero_f){
            e+=1;
        }
        e *= 2;

        printf("bit %d: %d %d -> %d \n", i, one_f, zero_f, e); 
    }
    return e / 2;
}


int main(int argc, char *argv[])
{
    FILE *in_file = fopen(argv[1], "r");

    char data[10000][64];
    size_t len;

    while (fscanf(in_file, "%s", data[len++]) != EOF);
    len--;
   
    char filter[64];
    filter[0] = 0;
    int g = Gamma(data, len, 5, 0, filter, 0);
    printf("%d \n", g);
    int e = Epsilon(data, len);
    printf("%d \n", e);
    
    int pc = e * g;

    printf("power consumption: %d \n", pc);

    fclose(in_file);
    exit(EXIT_SUCCESS);
}

