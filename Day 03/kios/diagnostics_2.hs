transposeString :: [String] -> [String]
transposeString ([]:_) = []
transposeString x = (map head x) : transposeString (map tail x)

count :: Char -> String -> Int
count x = length . filter (x==)

mostCommon :: String -> Char
mostCommon l = if ( count '1' l ) >= (count '0' l) then '1' else '0'

leastCommon :: String -> Char
leastCommon l = if ( count '0' l ) <= (count '1' l) then '0' else '1'

binToDec :: Int -> Int
binToDec 0 = 0
binToDec i = 2 * binToDec (div i 10) + (mod i 10)

sBinToDec :: String -> Int
sBinToDec  = binToDec . (read::String->Int)

selectM :: [String] -> Char
selectM l = mostCommon . head $ transposeString l

selectL :: [String] -> Char
selectL [x] = head x
selectL l = leastCommon . head $ transposeString l

oxygenF :: [String] -> [String]
oxygenF l = filter (\x -> head x == (selectM l) )  l

co2F :: [String] -> [String] 
co2F [x] = [x]
co2F l = filter (\x -> head x == (selectL l) ) l

oxygen :: [String] -> String
oxygen l = if all ("" ==) l then "" else selectM l : oxygen (map tail $ oxygenF l)

co2 :: [String] -> String
co2 l = if all ("" ==) l then "" else selectL l : co2 (map tail $ co2F l)

oxygenR :: [String] -> Int
oxygenR = sBinToDec . oxygen

co2R :: [String] -> Int
co2R = sBinToDec . co2


result :: [String] -> Int
result l = (oxygenR l) * (co2R l)

run file = 
    readFile file >>= print . result . words 
