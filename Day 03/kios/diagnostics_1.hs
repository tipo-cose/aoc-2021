transposeString :: [String] -> [String]
transposeString ([]:_) = []
transposeString x = (map head x) : transposeString (map tail x)

count :: Char -> String -> Int
count x = length . filter (x==)

mostCommon :: String -> Char
mostCommon l = if ( count '1' l ) >= (count '0' l) then '1' else '0'

leastCommon :: String -> Char
leastCommon l = if ( count '0' l ) <= (count '1' l) then '0' else '1'

binToDec :: Int -> Int
binToDec 0 = 0
binToDec i = 2 * binToDec (div i 10) + (mod i 10)

sBinToDec :: String -> Int
sBinToDec  = binToDec . (read::String->Int)

gamma :: [String] -> String
gamma  = map mostCommon . transposeString 

epsilon :: [String] -> String
epsilon = map leastCommon . transposeString

result :: [String] -> Int
result l = sBinToDec ( gamma l ) * sBinToDec ( epsilon l )

run file = 
    readFile file >>= print . result . words 
