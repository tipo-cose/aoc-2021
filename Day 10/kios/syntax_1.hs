import Data.Maybe (fromJust)
import Data.List (sort)


opening = ['(', '[', '{', '<']

opens :: Char -> Bool
opens c = c `elem` opening

closes :: Char -> Char -> Bool
closes b '(' = b == ')'
closes b '[' = b == ']'
closes b '{' = b == '}'
closes b '<' = b == '>'
closes _ _  = False

getClosing :: Char -> Char
getClosing '(' = ')'
getClosing '[' = ']'
getClosing '{' = '}'
getClosing '<' = '>'
getClosing _ = ' '


findCorruptedR :: String-> String -> Maybe Char
findCorruptedR [] _ = Nothing -- we have completed the string, but havent found a corrupting, so it's not corrupted
findCorruptedR (first:rest) open
    | opens first                = findCorruptedR rest ( first : open ) -- opens a new chunk
    | first `closes` head open   = findCorruptedR rest $ tail open      -- correctly closes this chunk
    | otherwise                  =  Just first                          -- if it neither opens nor close, it corrupts


findCorrupted :: String -> Maybe Char
findCorrupted s = findCorruptedR s ""


corrupted :: [String] -> [Char]
corrupted = map fromJust . filter (Nothing/=) . map findCorrupted


score :: Char -> Int
score ')' = 3
score ']' = 57
score '}' = 1197
score '>' = 25137
score x   = 0

scoreAll :: [String] -> [Int]
scoreAll = map score . corrupted

result :: [Int] -> Int
result =  sum

run file = 
    readFile file >>= print . result . scoreAll . lines
