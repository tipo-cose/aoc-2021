import Data.Maybe (fromJust)
import Data.List (sort)

opening = ['(', '[', '{', '<']

opens :: Char -> Bool
opens c = c `elem` opening

closes :: Char -> Char -> Bool
closes b '(' = b == ')'
closes b '[' = b == ']'
closes b '{' = b == '}'
closes b '<' = b == '>'
closes _ _  = False

getClosing :: Char -> Char
getClosing '(' = ')'
getClosing '[' = ']'
getClosing '{' = '}'
getClosing '<' = '>'
getClosing _ = ' '

findCompletingR :: String -> String -> String
findCompletingR [] open = open   -- if the string is empty but we still have opens, those are the incomplets
findCompletingR (first:rest) open
    | opens first                = findCompletingR rest ( first : open ) -- opens a new chunk
    | first `closes` head open   = findCompletingR rest $ tail open      -- correctly closes this chunk
    | otherwise                  =  ""                                   -- it's a corrupted! just ignore

-- convert the inconplete pairs to their closing char
findCompleting :: String -> String
findCompleting s = map getClosing $ findCompletingR s ""

-- get all the strings needed to complete ONLY incomplete strings
completions :: [String] -> [String]
completions = filter (not . null) . map findCompleting

-- scores a symbol
score :: Char -> Int
score ')' = 1
score ']' = 2
score '}' = 3
score '>' = 4
score x   = 0

scoreLine :: String -> Int
scoreLine = foldl (\f e -> f * 5 + score e) 0

middle :: [a] -> a
middle l = l !! (length l `div` 2)

result :: [String] -> Int
result = middle . sort . map scoreLine . completions

run file = 
    readFile file >>= print . result . lines