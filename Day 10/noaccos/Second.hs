import Data.List
import Data.Maybe
import Debug.Trace (trace)

parens = [('(',')'), ('[',']'), ('{','}'), ('<','>')]

fsts :: [(a,b)] -> [a]
fsts = map fst

snds :: [(a,b)] -> [b]
snds = map snd

isClosing :: Char -> Char -> Bool
isClosing c1 c2 = c2 == (fst . head . filter (\i -> (snd i==c1)) $ parens)

findIncomplete :: String -> [Char]
findIncomplete s = findIncompleteRec s []
  where
    findIncompleteRec [] opened = opened 
    findIncompleteRec (x:xs) opened
      | x `elem` fsts parens      = findIncompleteRec xs $ x : opened
      | isClosing x (head opened) = findIncompleteRec xs $ tail opened
      | otherwise                 = []

keepIncompletes :: [String] -> [String]
keepIncompletes = filter (not . null) . map findIncomplete

toValue :: String -> Int
toValue = foldl' (\acc x -> acc * 5 + values x) 0
  where
    values '(' = 1
    values '[' = 2
    values '{' = 3
    values '<' = 4

findMid :: [Int] -> Int
findMid l = (sort l) !! (length l `div` 2)

solve :: String -> IO ()
solve = print . findMid . map (toValue) . keepIncompletes . lines

mainBase = readFile "input-base" >>= solve
main = readFile "input" >>= solve
