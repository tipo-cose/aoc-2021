import Data.Maybe
import Debug.Trace (trace)

parens = [('(',')'), ('[',']'), ('{','}'), ('<','>')]

fsts :: [(a,b)] -> [a]
fsts = map fst

snds :: [(a,b)] -> [b]
snds = map snd

isClosing :: Char -> Char -> Bool
isClosing c1 c2 = c2 == (fst . head . filter (\i -> (snd i==c1)) $ parens)

findIllegal :: String -> Maybe Char
findIllegal s = findIllegalRec s []
  where
    findIllegalRec [] _ = Nothing
    findIllegalRec (x:xs) opened
      | x `elem` fsts parens      = findIllegalRec xs $ x : opened
      | isClosing x (head opened) = findIllegalRec xs $ tail opened
      | otherwise                 = Just x

removeIncomplete :: [Maybe Char] -> [Char]
removeIncomplete = map (fromMaybe 'a') . filter (/= Nothing)

solve :: String -> IO ()
solve = print . sum . map values . removeIncomplete . map findIllegal . lines
  where
    values ')' = 3
    values ']' = 57
    values '}' = 1197
    values '>' = 25137

mainBase = readFile "input-base" >>= solve
main = readFile "input" >>= solve
