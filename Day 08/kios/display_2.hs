import Data.List
import Data.Map (fromList)
import Data.Bits (Bits(xor))

wordsOn :: Char -> String -> [String]
wordsOn c s = case dropWhile (==c) s of
                "" -> []
                s' -> w : wordsOn c s''
                   where (w, s'') = break (==c) s'

count :: Int -> [Int] -> Int
count x =
    length . filter (x==)

uniqueLenghts = [2, 3, 4, 7]

genLines :: String -> [([String], [String])]
genLines = map ((\(f:s:_) -> (words f, words s)) . wordsOn '|') . lines

-- subtracts two lists, filtering the elements of list b from a
subtractL :: Eq a => [a] -> [a] -> [a]
subtractL = foldr delete

-- takes only strings of a certain length
takeX :: Int -> [String] -> String
takeX a = head . filter (\x -> length x == a)

-- The representation of one is the only one of lenght 2
takeOne :: [String] -> String
takeOne = takeX 2
-- The representation of seven is the only one of lenght 3
takeSeven :: [String] -> String
takeSeven = takeX 3
-- The representation of four is the only one of lenght 4
takeFour :: [String] -> String
takeFour = takeX 4
-- The representation of eight is the only one of lenght 7
takeEight :: [String] -> String
takeEight = takeX 7

-- get the top segments wire (`a): excluding the segments that make '1' from '7'
top :: [String] -> String
top x = subtractL (takeSeven x) (takeOne x)

-- get wires for right segments (`b,`c) (which are the same as '1')
right :: [String] -> String
right = takeOne

-- get wires for segments (`g, `f): excluding the segments that make '1' from '4'
up :: [String] -> String
up x = subtractL (takeFour x) (takeOne x)

-- get wires for segments (`d, `e): excluding the segments that make '1' and '4' from '8'
down :: [String] -> [Char]
down x = subtractL ( subtractL (takeEight x) (takeFour x)) (top x)

-- all possible block displacements
blocks :: [[String] -> String]
blocks = [block0, block1, block2, block3, block5, block6, block7, block8]
    where
        block0 x = top x ++ right x ++ down x ++ up x
        block1 x = top x ++ reverse (right x) ++ down x ++ up x
        block2 x = top x ++ right x ++ reverse (down x) ++ up x
        block3 x = top x ++ reverse (right x) ++ reverse (down x) ++ up x

        block5 x = top x ++ right x ++ down x ++ reverse (up x)
        block6 x = top x ++ reverse (right x) ++ down x ++ reverse (up x)
        block7 x = top x ++ right x ++ reverse (down x) ++ reverse (up x)
        block8 x = top x ++ reverse (right x) ++ reverse (down x) ++ reverse (up x)

-- generates an associative map function between segment representation and digit
constructAssociative :: String -> (String -> String)
constructAssociative x = k where k i
                                        | i == sort [x!!0, x!!1, x!!2, x!!3, x!!4, x!!5] = "0"
                                        | i == sort [x!!1, x!!2] = "1"
                                        | i == sort [x!!0 , x!!1 , x!!3 , x!!4 , x!!6] = "2"
                                        | i == sort [x!!0 , x!!1 , x!!6,  x!!2 , x!!3] = "3"
                                        | i == sort [x!!1 , x!!2 , x!!5 , x!!6] = "4"
                                        | i == sort [x!!0 , x!!2 , x!!3 , x!!5 , x!!6] = "5"
                                        | i == sort [x!!0 , x!!2 , x!!3 , x!!4 , x!!5 , x!!6] = "6"
                                        | i == sort [x!!0 , x!!1 , x!!2] = "7"
                                        | i == sort x = "8"
                                        | i == sort [x!!0 , x!!1 , x!!2 , x!!3 , x!!5 , x!!6] = "9"
                                        | otherwise = ""

-- decodes an entry using a specific associative function
decode :: (String -> String) -> [String] -> [String]
decode a  = map (a . sort)

-- tests the decoding capabilities of the displacement
testBlock :: [String] -> ([String] -> String) -> [String]
testBlock x f =
    decode k x
    where k = constructAssociative $ f x

-- tests all the displacements
testBlocks :: [String] -> [(Int, String)]
testBlocks x = zip (map (length . concat . testBlock x) blocks) (map (\i -> i x) blocks)

-- gets the only displacement that can decode all input encodings (aka 10)
bestBlock :: [String] -> String
bestBlock = head . map snd . filter (\x -> fst x == 10) . testBlocks

-- retries the associative of the corret block
getCorrectAssociative :: [String] -> (String -> String)
getCorrectAssociative x =
    constructAssociative $ bestBlock x

-- decodes an entire number
decodeNumber :: (String -> String) -> [String] -> String
decodeNumber a = concat . decode a

-- decodes a line, choosing the right displacement
decodeLine :: ([String], [String]) -> String
decodeLine (e, td) = decodeNumber (getCorrectAssociative e) td

decodeLines :: [([String], [String])] -> [String]
decodeLines = map decodeLine

result :: [String] -> Int
result = sum . map (read::String->Int)

run file =
    readFile file >>= print . result . decodeLines . genLines