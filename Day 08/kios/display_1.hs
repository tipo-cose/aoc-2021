wordsOn :: Char -> String -> [String]
wordsOn c s = case dropWhile (==c) s of
                "" -> []
                s' -> w : wordsOn c s''
                   where (w, s'') = break (==c) s'

count :: Int -> [Int] -> Int
count x =
    length . filter (x==)

uniqueLenghts = [2, 3, 4, 7]

genLines :: String -> [([String], [String])]
genLines = map ((\(f:s:_) -> (words f, words s)) . wordsOn '|') . lines

getDigitLenths :: [([String], [String])] -> [[Int]]
getDigitLenths = map (map length . snd)

countUnique :: [[Int]] -> [Int]
countUnique = map (sum . crs)
       where
        crs l = [count i l | i <- uniqueLenghts]

result :: [[Int]] -> Int
result = sum . countUnique

run file =
    readFile file >>= print . result . getDigitLenths. genLines
