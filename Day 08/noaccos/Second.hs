module Main where

import Data.List
import Data.List.Split
import qualified Data.Set as S
import qualified Data.Map as M

singlef :: (a -> Bool) -> [a] -> a
singlef p s = head $ filter p s

singlelen :: Int -> [S.Set Char] -> S.Set Char
singlelen len = singlef (\i -> length i == len)

shead :: S.Set a -> a
shead = S.elemAt 0

find1 :: [S.Set Char] -> S.Set Char
find1 = singlelen 2

find2 :: [S.Set Char] -> S.Set Char
find2 s = head $ filter (\i
  -> length i == 5
  && elem (getC s) i
  && i /= find3 s) s
  
find3 :: [S.Set Char] -> S.Set Char
find3 s = head $ filter (\i -> length i == 5 && find1 s `S.isSubsetOf` i) s

find4 :: [S.Set Char] -> S.Set Char
find4 = singlelen 4

find5 :: [S.Set Char] -> S.Set Char
find5 s = head $ filter (\i
  -> length i == 5
  && elem (getB s) i) s

find7 :: [S.Set Char] -> S.Set Char
find7 = singlelen 3

find8 :: [S.Set Char] -> S.Set Char
find8 = singlelen 7

find9 :: [S.Set Char] -> S.Set Char
find9 s = head $ filter (\i
  -> length i == 6
  && find1 s `S.isSubsetOf` i
  && S.fromList [getA s, getD s, getG s] `S.isSubsetOf` i) s

getA :: [S.Set Char] -> Char
getA s = shead $ S.difference (find7 s) (find1 s)

getB :: [S.Set Char] -> Char
getB s = shead $ S.difference (S.difference (find9 s) (find1 s)) (S.fromList [getA s, getD s, getG s])

getC :: [S.Set Char] -> Char
getC s = shead $ S.difference (find1 s) (S.fromList [getF s])

getD :: [S.Set Char] -> Char
getD s = shead $ S.difference (S.intersection (find4 s) (find3 s)) (find1 s)

getE :: [S.Set Char] -> Char
getE s = shead $ S.difference (find2 s) (S.fromList [getA s, getD s, getG s, getC s])

getF :: [S.Set Char] -> Char
getF s = shead $ S.difference (find5 s) $ S.fromList [getA s, getD s, getG s, getB s]

getG :: [S.Set Char] -> Char
getG s = shead $ S.difference (S.difference (find3 s) (find1 s)) (S.fromList [getA s, getD s])

decodeSegment :: [S.Set Char] -> Char -> Char
decodeSegment s c = M.findWithDefault 'a' c (segments s)
  where segments s = M.fromList [
          (getA s, 'a'),
          (getB s, 'b'),
          (getC s, 'c'),
          (getD s, 'd'),
          (getE s, 'e'),
          (getF s, 'f'),
          (getG s, 'g')]


decodeDigit :: [S.Set Char] -> S.Set Char -> Int
decodeDigit myset str = M.findWithDefault 0 (S.map (decodeSegment myset) str) digits
  where digits = M.fromList [
          (S.fromList "abcefg",  0),
          (S.fromList "cf",      1),
          (S.fromList "acdeg",   2),
          (S.fromList "acdfg",   3),
          (S.fromList "bcdf",    4),
          (S.fromList "abdfg",   5),
          (S.fromList "abdefg",  6),
          (S.fromList "acf",     7),
          (S.fromList "abcdefg", 8),
          (S.fromList "abcdfg",  9)]


composeNumber :: ([S.Set Char], [S.Set Char]) -> Int
composeNumber (inputs, outputs)
  = decodeDigit inputs (outputs !! 0) * 1000
  + decodeDigit inputs (outputs !! 1) * 100
  + decodeDigit inputs (outputs !! 2) * 10
  + decodeDigit inputs (outputs !! 3)

solve :: [([S.Set Char], [S.Set Char])] -> Int
solve = sum . map composeNumber

makeSets :: [String] -> [([S.Set Char],[S.Set Char])]
makeSets = map ((\[x,y] -> ( map S.fromList (words x), map S.fromList (words y))) . splitOn "|")

main :: IO ()
main = readFile "input" >>= print . solve . makeSets . lines
