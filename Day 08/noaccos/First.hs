import Data.List
import Data.List.Split

acceptedLengths = [2,4,3,7]

solve :: [String] -> Int
solve = length . filter (\i -> length i `elem` acceptedLengths)

main = readFile "input" >>= print . solve . words . concat . map (head . tail . splitOn "|") . lines
