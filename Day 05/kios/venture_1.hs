import Data.List
import Data.Maybe
import Data.Set (fromList, toList)

wordsOn :: Char -> String -> [String]
wordsOn c s = case dropWhile (==c) s of
                "" -> []
                s' -> w : wordsOn c s''
                   where (w, s'') = break (==c) s'

enumerate :: Eq a => Int -> [a] -> [(Int, a)]
enumerate _ [] = []
enumerate i (x:xs) = (i, x) : enumerate (i+1) xs

parseCoord = map (read::String->Int)

parseLine :: String -> [[Int]]
parseLine =  map ( parseCoord . wordsOn ',') . filter (/="->") . words

whateverRange :: Int -> Int -> [Int]
whateverRange s e
  | s == e = [e]
  | s > e = s : whateverRange (s-1) e
  | s < e = s : whateverRange (s+1) e

isHorizontal :: [Int] -> [Int] -> Bool
isHorizontal (ax:ay:_) (bx:by:_) = ay == by

isVertical :: [Int] -> [Int] -> Bool
isVertical (ax:ay:_) (bx:by:_) = ax == bx

generateVertical :: [Int] -> [Int] -> [(Int, Int)]
generateVertical (ax:ay:_) (bx:by:_) = [(ax, y) | y <- whateverRange ay by]

generateHorizontal :: [Int] -> [Int] -> [(Int, Int)]
generateHorizontal (ax:ay:_) (bx:by:_) = [(x, ay) | x <- whateverRange ax bx ]

generateDiagonal :: [Int] -> [Int] -> [(Int, Int)]
generateDiagonal (ax:ay:_) (bx:by:_) = zip (whateverRange ax bx) (whateverRange ay by)

generate :: [[Int]] -> [(Int, Int)]
generate (a:b:_)
    | isHorizontal a b = generateHorizontal a b
    | isVertical a b = generateVertical a b
    | otherwise = []


unique :: [(Int, Int)] -> [(Int, Int)]
unique = toList . fromList

count x = length . filter (x==)

countWhen xf = length . filter xf

countAll l = [count x l | x <- unique l]

getSegments = concatMap (generate . parseLine)

intersections = countWhen (>=2) . countAll

intersects (a,b) = a == b

run file =
    readFile file >>= print . intersections . getSegments . lines

run2 file =
    readFile file >>= print . map parseLine . lines

main = run "input"
