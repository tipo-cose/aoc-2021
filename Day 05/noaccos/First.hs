{-# LANGUAGE TupleSections #-}

import Data.List.Split
import Data.List
import Data.Function

toTuple :: [a] -> (a, a)
toTuple [x,y] = (x,y)

range :: Int -> Int -> [Int]
range x y
  | x > y = range y x
  | x == y = [x]
  | otherwise = x : range (x + 1) y

toPath :: ((Int,Int),(Int,Int)) -> [(Int,Int)]
toPath ((a,b),(c,d))
  | a == c = map (a,) $ range b d
  | b == d = map (,b) $ range a c

filterDiagonal :: [((Int,Int),(Int,Int))] -> [((Int,Int),(Int,Int))]
filterDiagonal = filter (\((a,b),(c,d)) -> a == c || b == d)

parseInput :: [String] -> [((Int,Int),(Int,Int))]
parseInput = map (toTuple . map ((read::String->(Int,Int)) . (\s -> "(" ++ s ++ ")" )) . splitOn " -> ")

solve :: [(Int,Int)] -> Int
solve = length . filter (\i -> length i > 1) . group . sortBy (compare `on` fst) . sortBy (compare `on` snd)

wrapper :: [String] -> IO ()
wrapper = print . solve . concat . map toPath . filterDiagonal . parseInput

main = readFile "input" >>= wrapper . lines
