
simulate :: (Integer, Integer) -> [String] -> (Integer, Integer)
simulate (d, p) [] = (d, p)
simulate (d, p) ("forward":x:xs) = simulate (d, p + (conv x)) xs 
simulate (d, p) ("up":x:xs)      = simulate (d - (conv x), p) xs 
simulate (d, p) ("down":x:xs)    = simulate (d + (conv x), p) xs 

conv :: String -> Integer
conv x = (read::String->Integer) x

-- return requested value from challenge
finalize :: (Integer, Integer) -> Integer
finalize (d, p) = d * p

run file = 
    readFile file >>= print . finalize . simulate (0, 0) . words
