#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// TESTED
int main(int argc, char *argv[])
{
    FILE *in_file = fopen(argv[1], "r");

    char command[10];
    int val;

    int position = 0, depth = 0, aim = 0;

    while (fscanf(in_file, "%s %d ", command, &val) != EOF) {
        if (strcmp(command, "forward") == 0){
            position += val;
            depth += aim * val;
        } else if (strcmp(command, "up") == 0){
           aim -= val;
        } else {
           aim += val;
        }

    }

    printf("OUT: %d \n", position * depth);

    fclose(in_file);
    exit(EXIT_SUCCESS);
}

