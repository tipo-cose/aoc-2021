
-- list of commmands, (depth, position, aim) -> (depth, position, aim)
simulate :: (Integer, Integer, Integer) -> [String] -> (Integer, Integer, Integer)
simulate (d, p, a) [] = (d, p, a)
simulate (d, p, a) ("up":x:xs)      = simulate (d, p, a - (conv x)) xs 
simulate (d, p, a) ("down":x:xs)    = simulate (d, p, a + (conv x)) xs 
simulate (d, p, a) ("forward":x:xs) = simulate (d + (a * (conv x)), p + (conv x), a) xs


conv :: String -> Integer
conv x = (read::String->Integer) x

finalize :: (Integer, Integer, Integer) -> Integer
finalize (d, p, _) = d * p

run file = do 
    content <- readFile file
    readFile file >>= print . finalize . simulate (0,0,0) . words
