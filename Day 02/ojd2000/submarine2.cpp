#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>

using std::string;

int main()
{
    auto in = std::ifstream{ "input" };
    int sum_fwd = 0, sum_depth = 0, aim = 0, tmp;
    string key;

    while (in >> key && in >> tmp)
    {
        if (key == "forward")
        {
            sum_fwd += tmp;
            sum_depth += aim * tmp;
        }
        
        else if (key == "down")
            aim += tmp;
        
        else
            aim -= tmp;
    }

    std::cout << sum_fwd * sum_depth << std::endl;
    return 0;
}