#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>

using std::string;

int main()
{
    auto in = std::ifstream{ "input" };
    int sum_fwd = 0, sum_depth = 0, tmp;
    string key;

    while (in >> key && in >> tmp)
    {
        if (key == "forward")
            sum_fwd += tmp;
        
        else if (key == "down")
            sum_depth += tmp;
        
        else
            sum_depth -= tmp;
    }

    std::cout << sum_fwd * sum_depth << std::endl;
    return 0;
}