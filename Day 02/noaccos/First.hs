import Data.Char

data Position = Zero | Forward Int Position | Down Int Position | Up Int Position
  deriving (Read, Show)

capitalize :: String -> String
capitalize ""     = ""
capitalize (x:xs) = toUpper x : xs

tupleSum :: (Int, Int) -> (Int, Int) -> (Int, Int)
tupleSum (a,b) (c,d) = (a+c, b+d)

solve :: Position -> (Int, Int)
solve Zero          = (0,0)
solve (Forward x p) = tupleSum (x,0)  $ solve p
solve (Up      x p) = tupleSum (0,-x) $ solve p
solve (Down    x p) = tupleSum (0,x)  $ solve p

wrapSolve :: String -> IO ()
wrapSolve = print . (\(a,b) -> a * b) . solve . (read::String->Position) . foldl (\acc x -> (capitalize x) ++ " (" ++ acc ++ ")") "Zero" . lines

main = readFile "input" >>= wrapSolve
