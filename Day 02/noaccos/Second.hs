import Data.Char

data Position = Zero | Forward Int Position | Down Int Position | Up Int Position
  deriving (Read, Show)

capitalize :: String -> String
capitalize ""     = ""
capitalize (x:xs) = toUpper x : xs

tupleSum :: (Int, Int) -> (Int, Int) -> (Int, Int)
tupleSum (a,b) (c,d) = (a+c, b+d)

aim :: Position -> Int
aim Zero = 0
aim (Forward _ p) = aim p
aim (Up x p) = (aim p) - x
aim (Down x p) = (aim p) + x

solve :: Position -> (Int, Int)
solve Zero          = (0,0)
solve (Forward x p) = tupleSum (x,x*(aim p))  $ solve p
solve (Up      x p) = solve p
solve (Down    x p) = solve p

wrapSolve :: String -> IO ()
wrapSolve = print . (\(a,b) -> a * b) . solve . (read::String->Position) . foldl (\acc x -> (capitalize x) ++ " (" ++ acc ++ ")") "Zero" . lines

main = readFile "input" >>= wrapSolve
