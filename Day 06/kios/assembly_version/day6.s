.data
format_int: .asciz "%lu\n"

input_data: 
    .quad 1
	.quad 1
	.quad 1
	.quad 3
	.quad 3
	.quad 2
	.quad 1
	.quad 1
	.quad 1
	.quad 1
	.quad 1
	.quad 4
	.quad 4
	.quad 1
	.quad 4
	.quad 1
	.quad 4
	.quad 1
	.quad 1
	.quad 4
	.quad 1
	.quad 1
	.quad 1
	.quad 3
	.quad 3
	.quad 2
	.quad 3
	.quad 1
	.quad 2
	.quad 1
	.quad 1
	.quad 1
	.quad 1
	.quad 1
	.quad 1
	.quad 1
	.quad 3
	.quad 4
	.quad 1
	.quad 1
	.quad 4
	.quad 3
	.quad 1
	.quad 2
	.quad 3
	.quad 1
	.quad 1
	.quad 1
	.quad 5
	.quad 2
	.quad 1
	.quad 1
	.quad 1
	.quad 1
	.quad 2
	.quad 1
	.quad 2
	.quad 5
	.quad 2
	.quad 2
	.quad 1
	.quad 1
	.quad 1
	.quad 3
	.quad 1
	.quad 1
	.quad 1
	.quad 4
	.quad 1
	.quad 1
	.quad 1
	.quad 1
	.quad 1
	.quad 3
	.quad 3
	.quad 2
	.quad 1
	.quad 1
	.quad 3
	.quad 1
	.quad 4
	.quad 1
	.quad 2
	.quad 1
	.quad 5
	.quad 1
	.quad 4
	.quad 2
	.quad 1
	.quad 1
	.quad 5
	.quad 1
	.quad 1
	.quad 1
	.quad 1
	.quad 4
	.quad 3
	.quad 1
	.quad 3
	.quad 2
	.quad 1
	.quad 4
	.quad 1
	.quad 1
	.quad 2
	.quad 1
	.quad 4
	.quad 4
	.quad 5
	.quad 1
	.quad 3
	.quad 1
	.quad 1
	.quad 1
	.quad 1
	.quad 2
	.quad 1
	.quad 4
	.quad 4
	.quad 1
	.quad 1
	.quad 1
	.quad 3
	.quad 1
	.quad 5
	.quad 1
	.quad 1
	.quad 1
	.quad 1
	.quad 1
	.quad 3
	.quad 2
	.quad 5
	.quad 1
	.quad 5
	.quad 4
	.quad 1
	.quad 4
	.quad 1
	.quad 3
	.quad 5
	.quad 1
	.quad 2
	.quad 5
	.quad 4
	.quad 3
	.quad 3
	.quad 2
	.quad 4
	.quad 1
	.quad 5
	.quad 1
	.quad 1
	.quad 2
	.quad 4
	.quad 1
	.quad 1
	.quad 1
	.quad 1
	.quad 2
	.quad 4
	.quad 1
	.quad 2
	.quad 5
	.quad 1
	.quad 4
	.quad 1
	.quad 4
	.quad 2
	.quad 5
	.quad 4
	.quad 1
	.quad 1
	.quad 2
	.quad 2
	.quad 4
	.quad 1
	.quad 5
	.quad 1
	.quad 4
	.quad 3
	.quad 3
	.quad 2
	.quad 3
	.quad 1
	.quad 2
	.quad 3
	.quad 1
	.quad 4
	.quad 1
	.quad 1
	.quad 1
	.quad 3
	.quad 5
	.quad 1
	.quad 1
	.quad 1
	.quad 3
	.quad 5
	.quad 1
	.quad 1
	.quad 4
	.quad 1
	.quad 4
	.quad 4
	.quad 1
	.quad 3
	.quad 1
	.quad 1
	.quad 1
	.quad 2
	.quad 3
	.quad 3
	.quad 2
	.quad 5
	.quad 1
	.quad 2
	.quad 1
	.quad 1
	.quad 2
	.quad 2
	.quad 1
	.quad 3
	.quad 4
	.quad 1
	.quad 3
	.quad 5
	.quad 1
	.quad 3
	.quad 4
	.quad 3
	.quad 5
	.quad 1
	.quad 1
	.quad 5
	.quad 1
	.quad 3
	.quad 3
	.quad 2
	.quad 1
	.quad 5
	.quad 1
	.quad 1
	.quad 3
	.quad 1
	.quad 1
	.quad 3
	.quad 1
	.quad 2
	.quad 1
	.quad 3
	.quad 2
	.quad 5
	.quad 1
	.quad 3
	.quad 1
	.quad 1
	.quad 3
	.quad 5
	.quad 1
	.quad 1
	.quad 1
	.quad 1
	.quad 2
	.quad 1
	.quad 2
	.quad 4
	.quad 4
	.quad 4
	.quad 2
	.quad 2
	.quad 3
	.quad 1
	.quad 5
	.quad 1
	.quad 2
	.quad 1
	.quad 3
	.quad 3
	.quad 3
	.quad 4
	.quad 1
	.quad 1
	.quad 5
	.quad 1
	.quad 3
	.quad 2
	.quad 4
	.quad 1
	.quad 5
	.quad 5
	.quad 1
	.quad 4
	.quad 4
	.quad 1
	.quad 4
	.quad 4
	.quad 1
	.quad 1
	.quad 2

ages: .fill 9, 8, 0

.text 
    .global main

print_int:
    push %rbx
    lea format_int(%rip), %rdi
    xor %rax, %rax
    call printf
    pop %rbx
    ret

main:
    xor %r15, %r15
    mov $input_data, %r14
    mov $ages, %r13
    jmp read_test

read_loop:
    movq (%r14, %r15, 8), %rbx
    movq (%r13, %rbx, 8), %rcx
    incq %rcx
    movq %rcx, (%r13, %rbx, 8)
    incq %r15
read_test:
    cmp $299, %r15
    jle read_loop

    mov %r13, %rsi
    xor %rax, %rax

    movq (%rsi, %rax, 8), %rbx 
    incq %rax
    movq (%rsi, %rax, 8), %r8 
    incq %rax
    movq (%rsi, %rax, 8), %r9 
    incq %rax
    movq (%rsi, %rax, 8), %r10 
    incq %rax
    movq (%rsi, %rax, 8), %r11 
    incq %rax
    movq (%rsi, %rax, 8), %r12 
    incq %rax
    movq (%rsi, %rax, 8), %r13 
    incq %rax
    movq (%rsi, %rax, 8), %r14 
    incq %rax
    movq (%rsi, %rax, 8), %r15 
    
    # days
    mov $256, %rcx

sim_loop:
    movq %rbx, %rdx
    movq %r8, %rbx
    movq %r9, %r8
    movq %r10, %r9
    movq %r11, %r10
    movq %r12, %r11
    movq %r13, %r12
    movq %r14, %r13
    addq %rdx, %r13 
    movq %r15, %r14
    movq %rdx, %r15
    
    decq %rcx
    jnz sim_loop
    
    xorq %rax, %rax
    addq %rbx, %rax
    addq %r8,  %rax
    addq %r9,  %rax
    addq %r10, %rax
    addq %r11, %rax
    addq %r12, %rax
    addq %r13, %rax
    addq %r14, %rax
    addq %r15, %rax

    movq %rax, %rsi
    call print_int

ret
