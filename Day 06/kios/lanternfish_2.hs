wordsOn :: Char -> String -> [String]
wordsOn c s = case dropWhile (==c) s of
                "" -> []
                s' -> w : wordsOn c s''
                   where (w, s'') = break (==c) s'

enumerate :: Eq a => Int -> [a] -> [(Int, a)]
enumerate _ [] = []
enumerate i (x:xs) = (i, x) : enumerate (i+1) xs

count :: Eq a => a -> [a] -> Int
count x = length . filter (==x)

simulateDay :: [Int] -> [Int]
simulateDay (i0:i1:i2:i3:i4:i5:i6:i7:i8:_) =
     [i1, i2, i3, i4, i5, i6, i7+i0, i8, i0]

get :: String -> [Int]
get = map (read::String->Int) . wordsOn ','

constructList :: [Int] -> [Int]
constructList l =
    map (`count` l) [0..8]

result :: (Foldable t, Num a) => t a -> a
result = sum

simulate :: Int -> [Int] -> [Int]
simulate 0 l = l
simulate x l = simulateDay $ simulate (x-1) l

run file days =
    readFile file >>= print . result . simulate days . constructList . get

main =
    run "input" 256
