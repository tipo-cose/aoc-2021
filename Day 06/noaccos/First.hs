import Data.List
import Data.List.Split

new :: [Int] -> [Int]
new xs = replicate (length (filter (==0) xs)) 8

transform :: [Int] -> [Int]
transform l = map (\i -> if i == 0 then 6 else i - 1) l ++ new l

solve :: Int -> [Int] -> [Int]
solve iters l
  | iters == 0 = l
  | otherwise = solve (iters - 1) $ transform l

wrapper :: [Int] -> IO ()
wrapper = print . length . solve 80

main = readFile "input" >>= wrapper . map (read::String->Int) . splitOn ","
