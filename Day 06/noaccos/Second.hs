import qualified Data.Map as M
import Data.List
import Data.List.Split

transform :: M.Map Int Int -> M.Map Int Int
transform m = M.delete (-1) . M.insertWith (+) 6 (M.findWithDefault 0 0 m) . M.insert 8 (M.findWithDefault 0 0 m) $ M.mapKeys (\i -> i - 1) m

solve :: Int -> M.Map Int Int -> M.Map Int Int
solve iters m
  | iters == 0 = m
  | otherwise = solve (iters - 1) $ transform m

wrapper :: M.Map Int Int -> IO ()
wrapper = print . sum . M.elems . solve 256

main = readFile "input" >>= wrapper . M.fromList . map (\l -> (head l, length l)) . group . sort . map (read::String->Int) . splitOn ","
