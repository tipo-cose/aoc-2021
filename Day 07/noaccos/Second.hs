import Data.List
import Data.List.Split

gauss :: Int -> Int
gauss n = n * (n+1) `div` 2

cost :: Int -> [Int] -> Int
cost guess = sum . map (\n -> gauss . abs $ n - guess)

-- Yes I know it's so bad, idc
solve :: Int -> Int -> [Int] -> Int
solve (-1) best l = best
solve guess best l
  | gv < best = solve (guess - 1) gv l
  | otherwise = solve (guess - 1) best l
  where gv = cost guess l

wrapper :: [Int] -> IO ()
wrapper l = print $ solve (maximum l) (gauss (maximum l) * gauss (maximum l)) l

main = readFile "input" >>= wrapper . map (read::String->Int) . splitOn ","
