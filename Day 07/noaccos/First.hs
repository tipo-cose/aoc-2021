import Data.List
import Data.List.Split

cost :: Int -> [Int] -> Int
cost guess = sum . map (abs . (guess-))

-- Yes I know it's so bad, idc
solve :: Int -> Int -> [Int] -> Int
solve (-1) best l = best
solve guess best l
  | gv < best = solve (guess - 1) gv l
  | otherwise = solve (guess - 1) best l
  where gv = cost guess l

wrapper :: [Int] -> IO ()
wrapper l = print $ solve (maximum l) (maximum l * maximum l) l

main = readFile "input" >>= wrapper . map (read::String->Int) . splitOn ","
