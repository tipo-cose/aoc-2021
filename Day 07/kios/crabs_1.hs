wordsOn :: Char -> String -> [String]
wordsOn c s = case dropWhile (==c) s of
                "" -> []
                s' -> w : wordsOn c s''
                   where (w, s'') = break (==c) s'

parseInput :: String -> [Int]
parseInput = map (read::String->Int) . wordsOn ','

singleCost :: Int -> Int -> Int
singleCost x y = abs (x - y)

costs :: Int -> [Int] -> [Int]
costs i = map (singleCost i)

cost :: Int -> [Int] -> Int
cost i = sum . costs i

best :: [Int] -> Int
best v = minimum $ map (`cost` v) [minimum v..maximum v]

run file =
    readFile file >>= print . best . parseInput
