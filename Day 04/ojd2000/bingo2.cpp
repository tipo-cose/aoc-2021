#include <algorithm>
#include <array>
#include <fstream>
#include <iostream>
#include <limits>
#include <map>
#include <numeric>
#include <optional>
#include <ranges>
#include <string>
#include <string_view>

using namespace std::string_view_literals;

namespace ranges = std::ranges;
namespace views = std::views;

using entry_type = short;

constexpr auto entry_max = std::numeric_limits<entry_type>::max();

int main()
{
    auto bingo = std::map<entry_type, size_t>{};
    auto in = std::ifstream{ "input" };
    std::string str;

    std::getline(in, str);

    for (size_t i = 0;
         short s : str
            | views::split(","sv)
            | views::transform([](auto&& part)
                {
                    auto c = part | views::common;
                    return std::stoi(std::string(c.begin(), c.end()));
                }))
        bingo.insert(std::make_pair(s, i++));
    
    auto board = std::array<std::array<entry_type, 5>, 5>{};
    auto in_view = ranges::istream_view<entry_type>(in);
    auto board_view = board | views::join;
    std::optional<entry_type> win_min;
    entry_type win_sum = 0;

    while (in)
    {
        ranges::copy_n(in_view.begin(),
                       25,
                       board_view.begin());

        // Inside copy_n's loop, in_view's iterator is incremented once more,
        // therefore it "steals" an integer.
        // It may be two digits, or a whitespace and a digit, so I unget twice.
        in.unget();
        in.unget();
        
        const auto all_in_bingo = [ &bingo ] (const auto& rowcol)
        {
            return ranges::all_of(rowcol,
                [ &bingo ] (auto e)
                {
                    return bingo.contains(e);
                });
        };
        const auto bingo_order_proj = [ &bingo ] (auto e)
        {
            return bingo[e];
        };

        auto min_rows_view = board
            | views::filter(all_in_bingo)
            | views::transform(
                [ bingo_order_proj ] (const auto& row)
                {
                    return *ranges::max_element(row,
                                                ranges::less{},
                                                bingo_order_proj);
                });

        auto min_entry = min_rows_view.empty()
            ? std::nullopt
            : static_cast<std::optional<entry_type>>(
                *ranges::min_element(min_rows_view,
                                     ranges::less{},
                                     bingo_order_proj));
        
        for (const auto col_idx : views::iota(0, 5))
        {
            const auto col = board
                | views::transform(
                    [ col_idx ] (const auto& row)
                    {
                        return row[col_idx];
                    });
            
            if (all_in_bingo(col))
            {
                const auto max_col = *ranges::max_element(col,
                                                          ranges::less{},
                                                          bingo_order_proj);

                min_entry = min_entry
                    ? ranges::min(*min_entry,
                                  max_col,
                                  ranges::less{},
                                  bingo_order_proj)
                    : max_col;
            }
        }

        if (min_entry
            && (!win_min
                || bingo[*min_entry] > bingo[*win_min]))
        {
            win_min = min_entry;

            const auto not_marked_acc = [ &bingo, min = *win_min ] (auto acc, auto e)
            {
                if (!bingo.contains(e)
                    || bingo[e] > bingo[min])
                    acc += e;
                
                return acc;
            };

            win_sum = std::reduce(board_view.begin(),
                                  board_view.end(),
                                  0,
                                  not_marked_acc);
        }
    }

    std::cout << win_sum * *win_min << std::endl;
    return 0;
}