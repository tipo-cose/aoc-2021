import Data.List
import Data.List.Split
import Data.Maybe

type Row = [Int]
type Card = [Row]

complete :: [Int] -> [Row] -> Maybe Row
complete l r =
  if null comp
  then Nothing
  else Just $ head comp
  where
    comp = filter (all (`elem` l)) r

winningCard :: Row -> [Card] -> Card
winningCard r c = fromMaybe [] $ find (elem r) c

getValue :: [Int] -> [Row] -> [Int] -> Int
getValue d c wr = sum . filter (`notElem` d) . concat . winningCard wr $ chunksOf 5 c

solvePartial :: [Int] -> [Card] -> Int -> Int
solvePartial d c n
  | isJust cr = getValue partiald (concat c)              (fromMaybe [] cr) * last partiald
  | isJust cc = getValue partiald (concatMap transpose c) (fromMaybe [] cc) * last partiald
  | otherwise = solvePartial d c $ n + 1
  where
    partiald = take n d
    cr = complete partiald $ concat c
    cc = complete partiald $ concatMap transpose c

mydel :: Eq a => a -> [a] -> [a]
mydel deleted xs = [ x | x <- xs, x /= deleted ]

solvePartial2 :: [Int] -> [Card] -> Int -> Int
solvePartial2 d c n
  | length c == 1 = solvePartial d c n
  | isJust cr = solvePartial2 d (mydel (winningCard (fromMaybe [] cr) c) c) n
  | isJust cc = solvePartial2 d (mydel (winningCard (fromMaybe [] cc) (map transpose c)) (map transpose c)) n
  | otherwise = solvePartial2 d c $ n + 1
  where
    partiald = take n d
    cr = complete partiald $ concat c
    cc = complete partiald $ concatMap transpose c

solve :: [Int] -> [Card] -> Int
solve d c =
  solvePartial2 d c 5

unwrappedCards :: [String] -> [[String]]
unwrappedCards l = splitOn [[]] (tail $ tail l)

cards :: [String] -> [Card]
cards = map (map (map( read::String->Int) . words) ) . unwrappedCards

draws :: [String] -> [Int]
draws l = map (read::String->Int) . splitOn "," $ head l

wrapper :: [String] -> IO ()
wrapper s = print $ solve (draws s) (cards s)

main :: IO ()
main = readFile "input" >>= wrapper . lines