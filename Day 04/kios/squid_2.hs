import Data.List
import Data.Maybe

wordsOn :: Char -> String -> [String]
wordsOn c s = case dropWhile (==c) s of
                "" -> []
                s' -> w : wordsOn c s''
                   where (w, s'') = break (==c) s'

enumerate :: Eq a => Int -> [a] -> [(Int, a)]
enumerate _ [] = []
enumerate i (x:xs) = (i, x) : enumerate (i+1) xs

maybeHead :: Eq a => [a] -> Maybe a
maybeHead [] = Nothing
maybeHead (x:_) = Just x

preparse :: [String] -> [String]
preparse = filter (/="")

getNumbers :: String -> [Int]
getNumbers = map (read::String->Int) . wordsOn ','

getTable :: [String] -> [[Int]]
getTable = map (map (read::String->Int) . words) . take 5

getTables :: [String] -> [[[Int]]]
getTables [] = []
getTables l = getTable l : getTables ( drop 5 l )

-- (number, board) -> row column or Nothing
findInTable :: Int -> [[Int]] -> Maybe (Int, Int)
findInTable x l = maybeHead . map (\(a,b) -> (a, fromJust b)) . filter (isJust . snd) . enumerate 0 $ map (elemIndex x) l

-- (number, boards) -> [(row, column) or Nothing]
findInTables :: Int -> [[[Int]]] -> [Maybe (Int, Int)]
findInTables e = map (findInTable e)

-- (position, with, list) -> list
replaceAt :: Int -> a -> [a] -> [a]
replaceAt _ _ [] = []
replaceAt 0 a (_:xs) = a:xs
replaceAt p a (x:xs)
    | p < 0 = x:xs
    | p > length (x:xs) = x:xs
    | otherwise = x : replaceAt (p - 1) a xs

-- ((row, column), board) -> board
maybeApplyTurn :: Maybe (Int, Int) -> [[Int]] -> [[Int]]
maybeApplyTurn Nothing xs = xs
maybeApplyTurn (Just t) xs = applyTurn t xs

-- ((row, column), board) -> board
applyTurn :: (Int, Int) -> [[Int]] -> [[Int]]
applyTurn (r, c) l = replaceAt r ( replaceAt c 0 (l!!r) ) l

-- board -> status
rowWin :: [[Int]] -> Bool
rowWin = any ((==True) . all (==0))

columnWin :: [[Int]] -> Bool
columnWin = rowWin . transpose

win :: [[Int]] -> Bool
win b = (rowWin b) || (columnWin b)

-- boards -> any winning
anyWin :: [[[Int]]] -> Bool
anyWin = any win

pruneWins :: [[[Int]]] -> [[[Int]]]
pruneWins = filter (not . win) 

getWinning :: [[[Int]]] -> [[Int]]
getWinning l = head $ filter win l

-- (extracted, board) -> score
score :: Int -> [[Int]] -> Int
score e b = e * sum ( concat b)

-- (extracted, boards) -> boards
turn :: Int -> [[[Int]]] -> [[[Int]]]
turn e bb = zipWith maybeApplyTurn (findInTables e bb) bb

-- last winning number, last winning boards, numbers left, boards -> winning boards, winning number
runTurnsLastWin :: Int -> [[[Int]]] -> [Int] -> [[[Int]]] -> ([[[Int]]], Int)
runTurnsLastWin  l wb []  _ = (wb, l)
runTurnsLastWin l wb (n:ns) b
    | anyWin next = runTurnsLastWin n next ns (pruneWins next)
    | otherwise = runTurnsLastWin l wb ns next
    where
        next = turn n b


process (b, e) = score e $ getWinning b

runGame n b = process $ runTurnsLastWin (head n) b n b


runFrom :: String -> Int
runFrom str =
    runGame nn bb
        where
            (n:ns) = lines str
            nn = getNumbers n
            bb = getTables $ preparse ns


run file =
    readFile file >>= print . runFrom
