import Data.List
import Data.Maybe

wordsOn :: Char -> String -> [String]
wordsOn c s = case dropWhile (==c) s of
                "" -> []
                s' -> w : wordsOn c s''
                   where (w, s'') = break (==c) s'

enumerate :: Eq a => Int -> [a] -> [(Int, a)]
enumerate _ [] = []
enumerate i (x:xs) = (i, x) : enumerate (i+1) xs

maybeHead :: Eq a => [a] -> Maybe a
maybeHead [] = Nothing
maybeHead (x:_) = Just x

preparse :: [String] -> [String]
preparse = filter (/="")

getNumbers :: String -> [Int]
getNumbers = map (read::String->Int) . wordsOn ','

getTable :: [String] -> [[Int]]
getTable = map (map (read::String->Int) . words) . take 5

getTables :: [String] -> [[[Int]]]
getTables [] = []
getTables l = getTable l : getTables ( drop 5 l )

findInTable :: Int -> [[Int]] -> Maybe (Int, Int)
findInTable x l = maybeHead . map (\(a,b) -> (a, fromJust b)) . filter (isJust . snd) . enumerate 0 $ map (elemIndex x) l

findInTables :: Int -> [[[Int]]] -> [Maybe (Int, Int)]
findInTables e = map (findInTable e)

replaceAt :: Int -> a -> [a] -> [a]
replaceAt _ _ [] = []
replaceAt 0 a (_:xs) = a:xs
replaceAt p a (x:xs)
    | p < 0 = x:xs
    | p > length (x:xs) = x:xs
    | otherwise = x : replaceAt (p - 1) a xs

maybeApplyTurn :: Maybe (Int, Int) -> [[Int]] -> [[Int]]
maybeApplyTurn Nothing xs = xs
maybeApplyTurn (Just t) xs = applyTurn t xs

applyTurn :: (Int, Int) -> [[Int]] -> [[Int]]
applyTurn (r, c) l = replaceAt r ( replaceAt c 0 (l!!r) ) l

rowWin :: [[Int]] -> Bool
rowWin = any ((==True) . all (==0))

columnWin :: [[Int]] -> Bool
columnWin = rowWin . transpose

win :: [[Int]] -> Bool
win b = (rowWin b) || (columnWin b)

anyWin :: [[[Int]]] -> Bool
anyWin = any win


score :: Int -> [[Int]] -> Int
score e b = e * sum ( concat b)

turn :: Int -> [[[Int]]] -> [[[Int]]]
turn e bb = zipWith maybeApplyTurn (findInTables e bb) bb



runTurns ::[Int] -> [[[Int]]] -> ([[[Int]]], Int)
runTurns []  _ = ([], -1)
runTurns (n:ns) b
    | anyWin next = (next, n)
    | otherwise = runTurns ns next
    where
        next = turn n b


process (b, e) = score e . head $ filter win b

runGame n b = process $ runTurns n b

runFrom :: String -> Int
runFrom str =
    runGame nn bb
        where
            (n:ns) = lines str
            nn = getNumbers n
            bb = getTables $ preparse ns

run file =
    readFile file >>= print . runFrom
