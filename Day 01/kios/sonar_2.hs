
sweep3 :: [Integer] -> Integer
sweep3 [] = 0
sweep3 [x] = 0
sweep3 [x, y] = 0
sweep3 [x, y, z] = 0
sweep3 (x:y:z:q:xs) = (upDown x y z q)
                + sweep3( [y, z, q] ++ xs)

-- return increment on a 3 size sliding window comparison
upDown :: Integer -> Integer -> Integer -> Integer -> Integer 
upDown x y z q = 
    if (y + z + q) - (x + y + z) > 0 then 1 else 0

run file = 
    readFile file >>= print . sweep3 . map (read::String->Integer) . words
