#include <stdio.h>
#include <stdlib.h>


// Tested
int main(int argc, char *argv[])
{
    FILE *in_file = fopen(argv[1], "r");

    int depths[10000]; 
    int incs = 0;
    size_t len = 0;

    while (fscanf(in_file, "%d", &depths[len++]) != EOF);

    for (size_t i = 0; i < len - 2; i++){
        if (depths[i + 1] - depths[i] > 0){
            ++incs;
        }
    }

    printf("Increments: %d \n", incs);

    fclose(in_file);
    exit(EXIT_SUCCESS);
}

