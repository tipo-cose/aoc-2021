#include <stdio.h>
#include <stdlib.h>


// Tested II
int main(int argc, char *argv[])
{
    FILE *in_file = fopen(argv[1], "r");

    int depths[10000]; 
    int incs = 0;
    int x, y, z, q;

    size_t len = 0;

    while (fscanf(in_file, "%d", &depths[len++]) != EOF);

    for (int i = 0; i < len - 4; i++){
        x = depths[i];
        y = depths[i+1];
        z = depths[i+2];
        q = depths[i+3];
        if ((y + z + q) - (x + y + z) > 0){
            ++incs;
        }
    }

    printf("Increments: %d \n", incs);

    fclose(in_file);
    exit(EXIT_SUCCESS);
}

