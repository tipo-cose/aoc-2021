
sweep :: [Integer] -> Integer
sweep [] = 0
sweep [x] = 0
sweep (x:y:xs) = ( upDown x y )
                + sweep( [y] ++ xs)

upDown :: Integer -> Integer -> Integer
upDown x y = if y - x > 0 then 1 else 0

run file = 
    readFile file >>= print . sweep . map (read::String->Integer) . words
