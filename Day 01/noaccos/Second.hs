-- [1, 2, 3] -> [1, 1, 2, 2, 3, 3]
duplicate :: [a] -> [a]
duplicate = reverse . foldl (\acc x -> x : x : acc) []

-- [1, 2, 3, 4, 5] -> [2, 3, 4]
body :: [a] -> [a]
body = tail . init

dupBody :: [a] -> [a]
dupBody x = head x : (duplicate $ body x) ++ [last x]

makeTupleList :: [a] -> [(a,a)]
makeTupleList []        = []
makeTupleList [x]       = [(x, x)]
makeTupleList (x:x1:xs) = (x,x1) : makeTupleList xs

sugarWereGoingDown :: (Int, Int) -> Int
sugarWereGoingDown (a, b)
  | a < b     = 1
  | otherwise = 0

solveFirst :: [Int] -> Int
solveFirst x = sum . map sugarWereGoingDown . makeTupleList $ dupBody x

makeTripleList :: [a] -> [(a,a,a)]
makeTripleList []        = []
makeTripleList [x]       = []
makeTripleList [x,y]     = []
makeTripleList (x:x1:x2:xs) = (x,x1,x2) : makeTripleList (x1 : x2 : xs)

solveSecond :: [Int] -> Int
solveSecond x = solveFirst . map (\(a,b,c) -> a+b+c) $ makeTripleList x

wrapsolve :: String -> IO ()
wrapsolve = putStrLn . show . solveSecond . map read . lines

main = readFile "input" >>= wrapsolve
