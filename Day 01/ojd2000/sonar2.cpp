#include <algorithm>
#include <array>
#include <fstream>
#include <iostream>

int main()
{
    auto in = std::ifstream{ "input" };
    auto measures = std::array<int, 4>{};
    int i = 0, j = 3, cnt = 0;

    std::copy_n(std::istream_iterator<int>{ in },
                3,
                measures.begin());

    while (in >> measures[j])
    {
        if (measures[j] > measures[i])
            cnt++;
        
        i = (i + 1) % 4;
        j = (j + 1) % 4;
    }

    std::cout << cnt << std::endl;
    return 0;
}