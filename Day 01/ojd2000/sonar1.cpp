#include <fstream>
#include <iostream>

int main()
{
    auto in = std::ifstream{ "input" };
    int prev, cur, cnt = 0;

    in >> prev;

    while (in >> cur)
    {
        if (cur > prev)
            cnt++;
        
        prev = cur;
    }

    std::cout << cnt << std::endl;
    return 0;
}