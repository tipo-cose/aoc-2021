import Data.List
import Data.List.Split
import Debug.Trace

data FoldType = Vertical | Horizontal
type Fold = (FoldType, Int)
type Point = (Int, Int)

toTuple :: [a] -> (a,a)
toTuple [x,y] = (x,y)

parsePoints :: [String] -> [Point]
parsePoints =
  map (toTuple . map (read :: String -> Int) . splitOn ",")

strToFold :: String -> Fold
strToFold s =
  (toFold $ head c, (read :: String -> Int) n)
    where
      [c, n] = splitOn "=" $ drop 11 s
      toFold 'x' = Horizontal
      toFold 'y' = Vertical

parseFolds :: [String] -> [Fold]
parseFolds = map strToFold

parse :: String -> ([Point], [Fold])
parse s =
  (parsePoints points, parseFolds folds)
    where [points, folds] = splitOn [""] $ lines s

foldCard :: Fold -> [Point] -> [Point]
foldCard (Vertical, n) = nub . afterFold
  where
    afterFold = map (\(x,y) ->
      if y <= n
      then (x, y)
      else (x, n + n - y))
foldCard (Horizontal,   n) = nub . afterFold
  where
     afterFold = map (\(x,y) ->
      if x <= n
      then (x,         y)
      else (n + n - x, y))

solve :: ([Point], [Fold]) -> Int
solve (p, (f:_)) = length $ foldCard f p

main' = readFile "example" >>= print . solve . parse
main  = readFile "input"   >>= print . solve . parse
