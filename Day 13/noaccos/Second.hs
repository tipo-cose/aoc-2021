import Data.List
import Data.List.Split
import Debug.Trace

data FoldType = Vertical | Horizontal
type Fold = (FoldType, Int)
type Point = (Int, Int)

toTuple :: [a] -> (a,a)
toTuple [x,y] = (x,y)

parsePoints :: [String] -> [Point]
parsePoints =
  map (toTuple . map (read :: String -> Int) . splitOn ",")

strToFold :: String -> Fold
strToFold s =
  (toFold $ head c, (read :: String -> Int) n)
    where
      [c, n] = splitOn "=" $ drop 11 s
      toFold 'x' = Horizontal
      toFold 'y' = Vertical

parseFolds :: [String] -> [Fold]
parseFolds = map strToFold

parse :: String -> ([Point], [Fold])
parse s =
  (parsePoints points, parseFolds folds)
    where [points, folds] = splitOn [""] $ lines s

foldCard :: Fold -> [Point] -> [Point]
foldCard (Vertical, n) = nub . afterFold
  where
    afterFold = map (\(x,y) ->
      if y <= n
      then (x, y)
      else (x, n + n - y))
foldCard (Horizontal,   n) = nub . afterFold
  where
     afterFold = map (\(x,y) ->
      if x <= n
      then (x,         y)
      else (n + n - x, y))

solve :: ([Point], [Fold]) -> Int
solve (p, (f:[])) = length $ trace (showPoints myF) myF
  where myF = foldCard f p
solve (p, (f:fs)) = trace (showPoints myF) (solve (myF, fs))
  where myF = foldCard f p

addNewLines :: String -> String
addNewLines "" = ""
addNewLines (c0:c1:c2:c3:c4:c5:c6:c7:c8:c9:c10:c11:c12:c13:c14:c15:c16:c17:c18:c19:c20:c21:c22:c23:c24:c25:c26:c27:c28:c29:c30:c31:c32:c33:c34:c35:c36:c37:c38:c39:c40:c41:c42:c43:c44:c45:c46:c47:c48:c49:c50:cs)
  = c0:c1:c2:c3:c4:c5:c6:c7:c8:c9:c10:c11:c12:c13:c14:c15:c16:c17:c18:c19:c20:c21:c22:c23:c24:c25:c26:c27:c28:c29:c30:c31:c32:c33:c34:c35:c36:c37:c38:c39:c40:c41:c42:c43:c44:c45:c46:c47:c48:c49:c50:'\n' : addNewLines cs
addNewLines a = a

showPoints :: [Point] -> String
showPoints l =
  addNewLines [
    if (x,y) `elem` l
    then '#'
    else '.'
  | y <- [0 .. 50]
  , x <- [0 .. 50]
  ]
  where
    maxX = maximum (map fst l)
    maxY = maximum (map snd l)

main' = readFile "example" >>= print . solve . parse
main  = readFile "input"   >>= print . solve . parse
