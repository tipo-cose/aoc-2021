import qualified Data.Map as M
import Data.Char
import Data.List
import Data.List.Split

toTuples :: [[String]] -> [(String, String)]
toTuples = map (\[x,y] -> (x,y))

makeMap :: [(String, String)] -> M.Map String [String]
makeMap = foldl' (\acc (a, b) ->
                    M.insertWith (++) a [b]
                   (M.insertWith (++) b [a] acc)) M.empty

parse :: String -> M.Map String [String]
parse = makeMap . toTuples . map (splitOn "-") . lines

big :: String -> Bool
big s = all isUpper s

isValid :: [String] -> String -> Bool
isValid l s
  | big s = True
  | s == "start" = False
  | otherwise = s `notElem` l

adjsPath :: [String] -> String -> M.Map String [String] -> [String]
adjsPath currentPath elem m = filter (isValid currentPath) $ M.findWithDefault [""] elem m

makePathsRec :: String -> String -> M.Map String [String] -> [String] -> [[String]]
makePathsRec start end m currentPath 
  | start == end = [[start]]
  | otherwise    = map ([start] ++) $ concat (map (\s -> makePathsRec s end m ([s] ++ currentPath)) (adjsPath currentPath start m))

makePaths :: String -> String -> M.Map String [String] -> [[String]]
makePaths start end m = makePathsRec start end m []

solve :: M.Map String [String] -> Int
solve = length . makePaths "start" "end"

main''' = readFile "example3" >>= print . solve . parse
main''  = readFile "example2" >>= print . solve . parse
main'   = readFile "example1" >>= print . solve . parse
main    = readFile "input"    >>= print . solve . parse
