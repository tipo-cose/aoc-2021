type IntM = [[Int]]
type Index = (Int, Int)

innerLen :: IntM -> Int
innerLen = length . head

inFilM :: (Index -> Bool) -> [[Int]] -> [Index]
inFilM f m = innerInFilM (0,0)
  where innerInFilM i@(x, y)
          | legalIndex m i =
	     if (f i)
             then i : innerInFilM (x + 1, y)
             else innerInFilM (x + 1, y)
          | x >= innerLen m = innerInFilM (0, y + 1)
          | otherwise = []

point :: IntM -> Index -> Int
point m (x, y) = m !! y !! x

legalIndex :: IntM -> Index -> Bool
legalIndex m (x, y) =
  x < innerLen m
  && y < length m
  && x >= 0
  && y >= 0

adjacentIndexes :: IntM -> Index -> [Index]
adjacentIndexes m (x, y) =
  filter (legalIndex m)
    [ (x, y - 1)
    , (x, y + 1)
    , (x - 1, y)
    , (x + 1, y) ]

adjs :: IntM -> Index -> [Int]
adjs m i =
  map (point m) $ adjacentIndexes m i

isLowPoint :: IntM -> Index -> Bool
isLowPoint m i =
  all (>cur) $ adjs m i
  where cur = point m i

solve :: IntM -> Int
solve m = sum . map (point m) $ inFilM (isLowPoint m) m

wrapper :: String -> IO ()
wrapper = print . solve
  . map (
    map ((+1) . (read :: String -> Int) . (:[])) ) . lines

mainBase :: IO ()
mainBase = readFile "input-base" >>= wrapper

main :: IO ()
main = readFile "input" >>= wrapper
