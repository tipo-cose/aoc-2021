import Data.List (nub, sort)

type IntM = [[Int]]
type Index = (Int, Int)

innerLen :: IntM -> Int
innerLen = length . head

inFilM :: (Index -> Bool) -> [[Int]] -> [Index]
inFilM f m = innerInFilM (0,0)
  where innerInFilM i@(x, y)
          | legalIndex m i =
             if (f i)
             then i : innerInFilM (x + 1, y)
             else innerInFilM (x + 1, y)
          | x >= innerLen m = innerInFilM (0, y + 1)
          | otherwise = []

point :: IntM -> Index -> Int
point m (x, y) = m !! y !! x

legalIndex :: IntM -> Index -> Bool
legalIndex m (x, y) =
  x < innerLen m
  && y < length m
  && x >= 0
  && y >= 0

adji :: IntM -> Index -> [Index]
adji m (x, y) =
  filter (legalIndex m)
    [ (x, y - 1)
    , (x, y + 1)
    , (x - 1, y)
    , (x + 1, y) ]

adjs :: IntM -> Index -> [Int]
adjs m i =
  map (point m) $ adji m i

isLowPoint :: IntM -> Index -> Bool
isLowPoint m i =
  all (>cur) $ adjs m i
  where cur = point m i

finals :: [[Index]] -> [Index]
finals = nub . map last

lowPoints :: IntM -> [Index]
lowPoints m = inFilM (isLowPoint m) m

mergeF :: IntM -> [[Index]] -> [[Index]]
mergeF m f = map (\i -> (nub $ concat [l | l <- f, i `elem` l])) (lowPoints m)

makeForest :: IntM -> [[Index]]
makeForest m = mergeF m . filter (/= []) $ innermkf (0,0) []
  where
    visited f i =
      i `elem` concat f  
    exploreTree i
      | isLowPoint m i = [i]
      | point m i == 9 = []
      | otherwise = i : (nub . concat $ map exploreTree ( filter ((< point m i) . (point m)) $ adji m i))
    innermkf i@(x,y) trees
      | legalIndex m i = innermkf (x + 1, y) $ (exploreTree i) : trees
      | x >= innerLen m = innermkf (0, y + 1) trees
      | otherwise = trees
    
solve :: IntM -> Int
solve = foldl1 (*) . take 3 . reverse . sort . map length . makeForest

wrapper :: String -> IO ()
wrapper = print
  . solve
  . map (
    map ((read :: String -> Int) . (:[])) ) . lines

mainBase :: IO ()
mainBase = readFile "input-base" >>= wrapper

main :: IO ()
main = readFile "input" >>= wrapper
