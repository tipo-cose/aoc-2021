type Dimension = Int 
type Size = (Dimension, Dimension)
type Value = Int
type MapC = [[Value]]
type MapL = [Value]
type Linear = Int 
type Cartesian = (Int, Int)

charToStr :: Char -> String
charToStr x = [x]

parseLine :: String -> [Value]
parseLine = map ((read::String->Int) . charToStr)

parse :: String -> MapC
parse = map parseLine . lines

-- returns valid index positions for the neightbours
safeIndeces :: Size -> Cartesian -> [Cartesian]
safeIndeces (w, h) (x, y)
-- corners
  | isTop    && isLeft  = [right, below]
  | isTop    && isRight = [left, below]
  | isBottom && isLeft  = [right, above]
  | isBottom && isRight = [left, above]
-- borders 
  | isLeft   = [above, right, below]
  | isRight  = [above, left, below]
  | isTop    = [left, below, right]
  | isBottom = [left, above, right]
-- general
  | otherwise = [above, right, below, left]
    where
      isTop    = y == 0
      isBottom = y == h - 1
      isLeft   = x == 0
      isRight  = x == w - 1

      above = (x, y - 1)
      below = (x, y + 1)
      left  = (x - 1, y)
      right = (x + 1, y)

-- converts a linear coordinate to a cartesian
linearToCart :: Size -> Linear -> Cartesian
linearToCart (w, h) x = (x `mod` w, x `div` w)

-- converts a cartesian coordinate to a linear
cartToLinear :: Size -> Cartesian -> Linear
cartToLinear (w, h) (x, y) = y * w + x

-- gets all the neighbor indeces, in linear coordinate space
getNeighborsPos :: Size -> MapL -> Linear -> [Linear]
getNeighborsPos s l x = 
    map (cartToLinear s) . safeIndeces s $ linearToCart s x

-- gets all neighbors
getNeighbors :: Size -> MapL -> Linear -> [Value]
getNeighbors s l x = 
    map (l!!) $ getNeighborsPos s l x

-- this point is the lowest among its neighbours
isLowest :: Size -> MapL -> Linear -> Bool
isLowest s l x =
    all (l!!x < ) $ getNeighbors s l x

-- all the lowest points
allLowest :: Size -> MapL -> [Value]
allLowest s l=
    map (l!!) $ filter (isLowest s l) [0..length l - 1]

-- wrapper 
findLowest :: MapC -> [Value]
findLowest m =
    allLowest s l
    where
        l = concat m
        w = length $ head m
        h = length m
        s = (w, h)

risk :: Value -> Int
risk x = x + 1

totalRisk :: [[Int]] -> Int
totalRisk = 
  sum . map risk . findLowest

run file =
    readFile file >>= print . totalRisk . parse
