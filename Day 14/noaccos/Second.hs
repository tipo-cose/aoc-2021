import Data.Function
import Data.List
import qualified Data.Map as M
type Rules = M.Map String String

fromPairs :: [String] -> String
fromPairs l = (concat $ map init (init l)) ++ last l

toPairs :: String -> [String]
toPairs "" = []
toPairs [x] = []
toPairs (x:xs@(y:ys)) = (x:[y]) : toPairs xs

splitPairs :: String -> [String]
splitPairs [a,b,c] = [[a,b], [b,c]]

toRule :: String -> (String, String)
toRule s = (from, to)
  where
    from   = take 2 s
    to     = head from : last s : last from : []

parseRules :: [String] -> Rules
parseRules r = M.fromList $ map toRule r

pairsToMap :: [String] -> M.Map String Int
pairsToMap = M.fromList . map (\l -> (head l, length l)) . group . sort

parse :: String -> (M.Map String Int, Rules, Char, Char)
parse s = (pairsToMap $ toPairs p, parseRules r, head p, last p)
  where
    l = lines s
    p = head l
    r = drop 2 l

    toMap = M.fromList (map (\l -> (head l, length l)) . group $ sort (toPairs p))

compress :: [(String, Int)] -> [(String, Int)]
compress = map (\l -> (fst $ head l, sum $ map snd l)). groupBy (\a b -> fst a == fst b) . sortBy (compare `on` fst)

newPairs :: Rules -> M.Map String Int -> M.Map String Int
newPairs rules pairs = M.fromList $ compress (concat $ map (\(s, i) -> (map (,i) . splitPairs $ (rules M.! s))) (M.toList pairs))

pyTimes :: (String, Int) -> String
pyTimes (s, i) = concat $ replicate i s

solve :: Int -> (M.Map String Int, Rules, Char, Char) -> Int
solve 0 (pairs, _, fstchr, lstchr) = most - fewer
  where
    s = sortBy (compare `on` snd) . map (\l -> (head l, (length l + (if (head l) `elem` [fstchr, lstchr] then 1 else 0)) `div` 2)) . group . sort . concat $ map pyTimes (M.toList pairs)

    l = M.toList (foldl (\acc ([k1,k2], v) -> M.insertWith (+) k1 v (M.insertWith (+) k2 v acc)) M.empty $ M.toList pairs)
    l2 = sortBy (compare `on` snd) $ map (\(k,v) -> (k, (v + if k `elem` [fstchr, lstchr] then 1 else 0) `div` 2)) l
    fewer = snd $ head l2
    most  = snd $ last l2

solve n (pairs, rules, fstchr, lstchr) = solve (n - 1) (newPairs rules pairs, rules, fstchr, lstchr)

main' = readFile "example" >>= print . solve 40 . parse
main  = readFile "input"   >>= print . solve 40 . parse
