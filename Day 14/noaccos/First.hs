import Data.Function
import Data.List
import qualified Data.Map as M
type Rules = M.Map String String

fromPairs :: [String] -> String
fromPairs l = (concat $ map init (init l)) ++ last l

toPairs :: String -> [String]
toPairs "" = []
toPairs [x] = []
toPairs (x:xs@(y:ys)) = (x:[y]) : toPairs xs

splitPairs :: String -> [String]
splitPairs [a,b,c] = [[a,b], [b,c]]

toRule :: String -> (String, String)
toRule s = (from, to)
  where
    from   = take 2 s
    to     = head from : last s : last from : []

parseRules :: [String] -> Rules
parseRules r = M.fromList $ map toRule r

parse :: String -> ([String], Rules)
parse s = (toPairs p, parseRules r)
  where
    l = lines s
    p = head l
    r = drop 2 l

solve :: Int -> [String] -> Rules -> Int
solve 0 pairs _ = most - fewer
  where
    s = sortBy (compare `on` snd) . map (\l -> (head l, length l)) . group . sort $ fromPairs pairs
    fewer = snd $ head s
    most  = snd $ last s

solve n pairs rules = solve (n - 1) newPairs rules
  where
    newPairs = concat $ map (splitPairs . (rules M.!)) pairs

main' = readFile "example" >>= print . uncurry (solve 10) . parse
main  = readFile "input"   >>= print . uncurry (solve 10) . parse
