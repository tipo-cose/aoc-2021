import Data.List
import Data.Function
import Debug.Trace

type IntM = [[Int]]
type Index = (Int, Int)

innerLen :: IntM -> Int
innerLen = length . head

inFilM :: (Index -> Bool) -> IntM -> [Index]
inFilM f m = innerInFilM (0,0)
  where innerInFilM i@(x, y)
          | legalIndex m i =
             if (f i)
             then i : innerInFilM (x + 1, y)
             else innerInFilM (x + 1, y)
          | x >= innerLen m = innerInFilM (0, y + 1)
          | otherwise = []

inFilMP :: (Int -> Bool) -> IntM -> [Index]
inFilMP f m = inFilM (f . (point m)) m

numOf :: (Index -> Bool) -> IntM -> Int
numOf f = length . inFilM f

countPoints :: (Int -> Bool) -> IntM -> Int
countPoints f m = numOf (f . (point m)) m

myChunksOf :: Int -> [a] -> [[a]]
myChunksOf _ [] = []
myChunksOf n l  = (take n l) : (myChunksOf n $ drop n l)

mapM :: (a -> b) -> [[a]] -> [[b]]
mapM f = map (map f)

marshall :: Index -> IntM -> Int
marshall (a,b) m = a + (innerLen m) * b

unmarshall :: Int -> IntM -> Index
unmarshall x m = (x `mod` innerLen m, x `div` innerLen m)

mapinrec :: (Index -> Int -> Int) -> [Int] -> Index -> Int -> [Int]
mapinrec _ []     _            _      = []
mapinrec f (x:xs) (inda, indb) rowLen = (f (inda,indb) x) : mapinrec f xs nextIndex rowLen
  where
    nextIndex
      | inda == rowLen - 1 = (0, 1 + indb)
      | otherwise          = (1 + inda, indb)

mapIndex :: (Index -> Int -> Int) -> IntM -> IntM
mapIndex f m = myChunksOf n $ mapinrec f (concat m) (0,0) n
  where n = innerLen m

point :: IntM -> Index -> Int
point m (x, y) = m !! y !! x

legalIndex :: IntM -> Index -> Bool
legalIndex m (x, y) =
  x < innerLen m
  && y < length m
  && x >= 0
  && y >= 0

adji :: IntM -> Index -> [Index]
adji m (x, y) =
  filter (legalIndex m)
    [ (x - 1, y - 1)
    , (x,     y - 1)
    , (x + 1, y - 1)
    , (x - 1, y)
    , (x + 1, y)
    , (x - 1, y + 1)
    , (x,     y + 1)
    , (x + 1, y + 1) ]

adjs :: IntM -> Index -> [Int]
adjs m i =
  map (point m) $ adji m i

fstWrap :: a -> (a, IntM)
fstWrap a = (a, [[]])

sumFirst :: Num a => (a,b) -> (a,b) -> (a,b)
sumFirst (a,_) (c,d) = (a+c, d)

maxLvl :: IntM -> [Index]
maxLvl = inFilMP (>9)

groupIndexes :: [Index] -> [[Index]]
groupIndexes = group . sortBy (compare `on` fst) . sortBy (compare `on` snd)

getIncrements :: IntM -> [(Index, Int)]
getIncrements m = map (\i -> (head i, length i)) . groupIndexes . concat $ map (adji m) (maxLvl m)

sndIn :: Eq a => a -> [(a,b)] -> b
sndIn item ((a,b):xs)
  | item == a = b
  | otherwise  = sndIn item xs

incrementM :: IntM -> [(Index, Int)] -> IntM
incrementM m l =
  mapIndex
  (\index item ->
  if index `elem` map fst l && item /= -1
  then item + sndIn index l
  else item) $
  mapIndex
  (\index item ->
  if item > 9
  then -1
  else item) m

transform :: IntM -> IntM
transform m = incrementM m $ getIncrements m

oneStep :: IntM -> (Int, IntM)
oneStep m
  | countPoints (>9) m == 0 =
    (0, Main.mapM (\item -> if item == -1 then 0 else item) m)
  | otherwise = 
    sumFirst
      (fstWrap $ countPoints (>9) m)
      (oneStep $ transform m)

solve :: IntM -> Int
solve m
  | flashes == n = 2
  | otherwise    = 1 + solve newmatrix
  where
    (flashes, newmatrix) = oneStep (Main.mapM (+1) m)
    n = length m * innerLen m

prep :: String -> IntM
prep = Main.mapM ((+1) . (read :: String -> Int) . (:[])) . lines

mainBase :: String -> IO ()
mainBase s = readFile s >>= print . solve . prep

mainEx = mainBase "example"
main   = mainBase "input"
